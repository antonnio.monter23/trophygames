<!DOCTYPE html>
<html class="loading" lang="es" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Anfad | Panel {{$title }}</title>
    <link rel="apple-touch-icon" href="{{asset('assets/images-new/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images-new/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- META TAGS -->
    <meta property="og:site_name" content="Akademus">
    <meta property="og:title" content="Akademus" />
    <meta property="og:description" content="Administración Colegio Amaranto" />
    <meta property="og:image" itemprop="image" content="{{asset('assets/images-new/apple-icon-120.png')}}">
    <meta property="og:type" content="website" />
    <meta property="og:updated_time" content="1440432930" />

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/material-vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/js/gallery/photo-swipe/photoswipe.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/weather-icons/climacons.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/meteocons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/charts/morris.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/charts/chartist.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/charts/chartist-plugin-tooltip.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/forms/icheck/custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/extensions/toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/extensions/sweetalert.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/forms/selects/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/calendars/fullcalendar.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/forms/toggle/bootstrap-switch.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/forms/toggle/switchery.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/material.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/material-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/material-colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/material-vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/menu/menu-types/material-vertical-content-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/gallery.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/simple-line-icons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/timeline.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/dashboard-ecommerce.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/forms/checkboxes-radios.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/extensions/toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/users.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/calendars/fullcalendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/forms/switch.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/colors/palette-switch.css')}}">
    

    <!-- END: Page CSS-->

    

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom.css')}}">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-content-menu material-vertical-layout material-layout 2-columns fixed-navbar" data-open="click" data-menu="vertical-content-menu" data-col="2-columns">

    <style type="text/css">
        html body{
            /*
            background-color: #243640;
            background: linear-gradient(to left, #2292C2 0%, #6FC4E9 100%);
            */
            background: linear-gradient(to left, #243640 0%, #243640 100%);
        }
        html body .content{
            min-height: calc(140% - 32px);
        }
        .pattern {
            background: url(../../assets/images-new/pattern6.png) fixed;
        }
        .pattern-two{
            background: url(../../assets/images-new/pattern2.png);
        }
        .header-navbar .navbar-header .navbar-brand .brand-logo {
            width: 150px;
            margin-top: -5px;
        }
        .material-vertical-layout .main-menu.menu-light .user-profile {
            background: linear-gradient(to left, #008bcc 0%, #374a9c 100%);
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            border-bottom: 2px solid #fff;
        }
        .material-vertical-layout .main-menu .main-menu-content .navigation li.active > a {
            background-color: #304594;
            color: #FFFFFF;
        }
        .user-info{
            background: url(../../assets/images-new/pattern2.png);
        }
        .user-name{
            color: #fff;
            text-shadow: 2px 2px 10px #000;
            font-size: 16px;
        }
        .user-rol{
            color: #82e5ff;
            text-decoration: underline;
            text-shadow: 2px 2px 10px #000;
        }
        .material-vertical-layout .main-menu .user-profile .user-info .user-img {
            width: 100px;
            height: 100px;
            box-shadow: 2px 2px 10px #000;
        }
        .material-vertical-layout .main-menu.menu-light {  
            border: 2px solid #fff !important;
            border-radius: 5px !important;
        }
        .navbar-dark{
            background: linear-gradient(to left, #008bcc 0%, #304594 100%);
            border-bottom: 2px solid #fff !important;
        }
        .footer-dark{
            background: linear-gradient(to left, #008bcc 0%, #304594 100%) !important;
        }
        .main-menu.menu-light .navigation > li:hover > a{
            color: #008bcc;
        }
        .exit a:hover{
            background-color: #c70039 !important;
            color: #fff !important;
        }
        .btn-primary{
            background-color: #304594 !important;
        }
        .btn-info{
            background-color: #149fbb !important;
        }
    </style>

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-dark  navbar-border navbar-shadow navbar-brand-center">
        <div class="navbar-wrapper pattern-two">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item"><a class="navbar-brand" href="index.html"><img class="brand-logo" src="{{asset('assets/images-new/logo-white.png')}}"></a></li>
                    <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="material-icons mt-1">more_vert</i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                    </ul>
                    <ul class="nav navbar-nav float-right">
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->

    <!-- BEGIN: Content-->
    <div class="app-content content pattern">
        <div class="content-header row">
        </div>
        <div class="content-wrapper">

            <!-- BEGIN: Main Menu-->

            <!-- END: Main Menu-->
            <div class="content-body">
                @yield('contenido')
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <div class="footer-dark">
        <footer class="footer footer-static navbar-border pattern-two">
            <p class="clearfix text-white lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Derechos Reservados &copy; 2020 Akademus</span><span class="float-md-right d-none d-lg-block">Powered by Proelium | Made with <i class="ft-heart white"></i><span id="scroll-top"></span></span></p>
        </footer>
    </div>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('assets/vendors/js/material-vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/vendors/js/ui/headroom.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/gallery/masonry/masonry.pkgd.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/gallery/photo-swipe/photoswipe.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/gallery/photo-swipe/photoswipe-ui-default.min.js')}}"></script>
    <script src="{{asset('assets/js/scripts/gallery/photo-swipe/photoswipe-script.js')}}"></script>
    <script src="{{asset('assets/vendors/js/charts/chartist.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/charts/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/charts/raphael-min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/charts/morris.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/timeline/horizontal-timeline.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/toastr.min.js')}}"></script>

    <script src="{{asset('assets/vendors/js/ui/headroom.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/select/select2.full.min.js')}}"></script>

    <script src="{{asset('assets/vendors/js/editors/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/toggle/bootstrap-switch.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/toggle/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/js/scripts/pages/material-app.js')}}"></script>
    <script src="{{asset('assets/js/scripts/forms/checkbox-radio.js')}}"></script>
    <script src="{{asset('assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script>
    <script src="{{asset('assets/js/scripts/extensions/toastr.js')}}"></script>
    <script src="{{asset('assets/js/scripts/forms/select/form-select2.js')}}"></script>
    <script src="{{asset('assets/js/scripts/modal/components-modal.js')}}"></script>

    <script src="{{asset('assets/js/scripts/pages/material-app.js')}}"></script>
    <script src="{{asset('assets/js/scripts/tables/datatables-extensions/datatables-sources.js')}}"></script>
    <script src="{{asset('assets/js/scripts/extensions/sweet-alerts.js')}}"></script>

    <script src="{{asset('assets/js/scripts/pages/material-app.js')}}"></script>
    <script src="{{asset('assets/js/scripts/editors/editor-ckeditor.js')}}"></script>

    
    <script src="{{asset('assets/js/scripts/forms/select/form-select2.js')}}"></script>   
    
    <!-- END: Page JS-->

    <script src="{{asset('assets/js/custom.js')}}"></script>

    @if (isset($impr) && $impr == '1')
		<script>
			$(document).ready(function () {
                toastr.{{$vary[0]}}(
                    '{{$vary[1]}}', 
                    '{{$vary[2]}}', 
                    {
                        "closeButton": true
                    }
                );
			});
		</script>
    @endif
    

</body>
<!-- END: Body-->

</html>