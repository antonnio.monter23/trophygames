<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Sistema de Administración">
    <meta name="author" content="ANFAD">
    <title>ANFAD | Administración</title>
    <link rel="apple-touch-icon" href="{{asset('assets/images-anfad/social-icon.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images-anfad/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/material-icons/material-icons.css')}}">

    <!-- META TAGS -->
    <meta property="og:site_name" content="ANFAD">
    <meta property="og:title" content="ANFAD" />
    <meta property="og:description" content="Sistema de Administración" />
    <meta property="og:image" itemprop="image" content="{{asset('assets/images-anfad/social-icon.png')}}">
    <meta property="og:type" content="website" />
    <meta property="og:updated_time" content="1440432930" /> 

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/material-vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/forms/icheck/custom.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/material.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/material-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/material-colors.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/menu/menu-types/material-vertical-content-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/login-register.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="color-body">

    <style type="text/css">
        html body .pace .pace-progress {
            background: transparent; 
        }
        .color-body {
            background: linear-gradient(120deg, #4a4a4a 25%, #3e3e3e 25%, #3e3e3e 50%, #4a4a4a 50%, #4a4a4a 75%, #3e3e3e 75%, #3e3e3e);
        }
        .card-login {
            background-color: #e6e7e9;
            top: 17%;
            border-radius: 0;
            border: 2px solid #ab6b5d;
            box-shadow: 2px 2px 15px #000;
        }
        .card-login .card-header {
            padding-bottom: 0;
        }
        .logo-login {
            width: 150px;
            margin-top: 10px;
            -webkit-filter: drop-shadow(1px 1px 0px gray);
        }
        .text-login {
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
            margin-top: 15px;
            margin-bottom: 10px;
            background: #52585c;
            border-radius: 3px;
        }
        .form-group {
            background-color: #fffdf9;
        }
        .form-control-position {
            line-height: 2.6rem;
        }
        .position-relative.has-icon-left .form-control {
            padding-left: 2.7rem;
        }
        .form-control-position i{
            color: #e94d2a;
        }
        .forgot {
            padding-top: 0px;
            padding-bottom: 8px;
        }
        .forgot a {
            color: #404140;
            text-decoration: underline;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .forgot a:hover {
            color: #e94d2a;
        }
        .btn {
            font-weight: bold;
            border-radius: 0 !important;
        }
        .btn-mail i{
            vertical-align: -0.1rem;
        }
        .btn-primary {
            background-color: #e94d2a !important;
            color: #fff !important;
        }
        .btn-primary:hover {
            background-color: #ab6b5d !important;
            color: #fff !important;
        }
        .btn-primary:focus {
            background-color: #ab6b5d !important;
            color: #fff !important;
        }
        .btn-primary:active {
            background-color: #ab6b5d !important;
            color: #fff !important;
        }
        .credits {
            margin-top: 90px;
        }
        .alert-warning {
            border-color: #ffc107 !important;
            background-color: #ffc107;
        }
        .alert {
            color: #000;
        }
        @media  screen and (max-width: 980px) {
            .card-login {
                top: 10%;
            }
            .line-on-side {
                line-height: 0rem;
            }
            .credits {
                margin-top: 40px;
            }
        }
    </style>
    
    <!-- BEGIN: Content-->
    @yield('contenido')  
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('assets/vendors/js/material-vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/vendors/js/ui/headroom.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/js/scripts/pages/material-app.js')}}"></script>
    <script src="{{asset('assets/js/scripts/forms/form-login-register.js')}}"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>