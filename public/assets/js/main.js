/*var ruta_sitio = window.location.hostname;
var path = '';
if(ruta_sitio == '127.0.0.1'){
    path = 'http://127.0.0.1:8000/';
} else {
    path = 'https://www.proyectosweb.mx/demos/anfad-asociados/';
}*/
Dropzone.options.dpzMultipleFiles = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 5, // MB
    acceptedFiles: 'image/*',
    clickable: true,
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    init: function() {

        // Using a closure.
        var _this = this;

        // Setup the observer for the button.
        $("#clear-dropzone").on("click", function() {
            // Using "_this" here, because "this" doesn't point to the dropzone anymore
            _this.removeAllFiles();
            // If you want to cancel uploads as well, you
            // could also call _this.removeAllFiles(true);
        });
    }
}

$(document).ready(function () {

    //Iniciando el plugin de dataTables: inicia mostranto 20 registros
    $('#table-list').DataTable({
        "autoWidth": true,
        "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "Todos"]]
    });

    
});

$(".cambiarOrden").sortable();

$(".cambiarOrden").on("sortupdate", function(event, ui){
    
    var $data = $(this).sortable("serialize");
    var $data_url = $(this).data("url");
    $.get($data_url, {data : $data}, function(response){
        for(var i=0; i<response.length; i++) {
            $(".orden-"+response[i]).load(" .orden-"+response[i]);
            $(".loop-"+response[i]).load(" .loop-"+response[i]);
        }
        toastr.success('Cambio el orden del listado', 'Modificando datos', { "closeButton": true });
    });
});

$( ".genera-pass" ).click(function() {
    var $data_url = $(this).data("url");
    $.get($data_url, function(response){
        $("#asuClave").val(response);
    });
});

function fileUsuario() {
    var archivoInput = document.getElementById('usrAvatar');
    var archivoRuta = archivoInput.value;
    var extPermitidas = /(.jpg|.png)$/i;
    var sizeByte = archivoInput.files[0].size;
    var siezekiloByte = parseInt(sizeByte / 1024);

    const $seleccionArchivos = document.querySelector("#usrAvatar");
    const $imagenPrevisualizacion = document.querySelector("#preview");

    if (siezekiloByte > $(archivoInput).attr('size')) {
        $('#form-evArchivo').addClass('form-error');
        $('#helper-usrAvatar').html('Peso de documento no soportado, el peso del documento supera el límite permitido (5 megas)');
        $('#helper-usrAvatar').css("display", "block");
        archivoInput.value = '';
        $imagenPrevisualizacion.src = "";
        return false;
    } else {
        $('#form-usrAvatar').removeClass('form-error');
        $('#helper-usrAvatar').html('');
        $('#helper-usrAvatar').css("display", "none");
    }

    if (!extPermitidas.exec(archivoRuta)) {
        $('#form-usrAvatar').addClass('form-error');
        $('#helper-usrAvatar').html('Formato no soportado, segurese de haber seleccionado un archivos con extención jpg o png');
        $('#helper-usrAvatar').css("display", "block");
        archivoInput.value = '';
        $imagenPrevisualizacion.src = "";
        return false;
    } else {
        $('#form-usrAvatar').removeClass('form-error');
        $('#helper-usrAvatar').html('');
        $('#helper-usrAvatar').css("display", "none");

        const archivos = $seleccionArchivos.files;

        const primerArchivo = archivos[0];
        // Lo convertimos a un objeto de tipo objectURL
        const objectURL = URL.createObjectURL(primerArchivo);
        // Y a la fuente de la imagen le ponemos el objectURL
        $imagenPrevisualizacion.src = objectURL;

    }
}

function validarExt(idEv) {
    var archivoInput = document.getElementById(idEv);
    var archivoRuta = archivoInput.value;
    var extPermitidas = /(.pdf)$/i;
    var sizeByte = archivoInput.files[0].size;
    var siezekiloByte = parseInt(sizeByte / 1024);

    if(siezekiloByte > $(archivoInput).attr('size')){
        toastr.error('El peso del archivo supera el límite permitido (5 megas)', 'Peso del archivo no soportado', { "closeButton": true });
        archivoInput.value = '';
        return false;
    }

    if (!extPermitidas.exec(archivoRuta)) { 
        toastr.error('Asegurese de haber seleccionado un archivos con extención .pdf', 'Formato no soportado', { "closeButton": true });
        archivoInput.value = '';
        return false;
    }
}

function recargaGaleria() {
    $('#card-galeria').load(' #card-galeria');
}