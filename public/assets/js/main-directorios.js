$(document).ready(function () {

    $(".isActiveDirectorio").change(function () {
        var $data = $(this).prop("checked");
        var $data_url = $(this).data("url");

        if (typeof $data !== "undefined" && typeof $data_url !== "undefined") {
            $.get($data_url, { data: $data }, function (response) {
                $("#refresh-status").load(" #refresh-status");
                toastr.success('El estatus cambio correctamente', 'Modificando datos', { "closeButton": true });
            });
        }
    });

});
