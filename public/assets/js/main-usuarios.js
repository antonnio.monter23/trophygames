//Editamos los datos de acceso
$(".btnEditarDatosAcceso").click(function () {
    var path = $(this).data("url");

    var token = $("#token").val();
    var usrEmail = $('#usrEmail').val();
    var usrClave = $('#usrClave').val();

    $.ajax({
        url: path,
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        dataType: 'json',
        data: {
            usrEmail: usrEmail,
            usrClave: usrClave
        },
        success: function (response) {
            //alert(response);
            console.log(response);
            //location.reload();
            //$("#error-email-tutor").fadeIn();

            if (response.status == 'success') {
                toastr.success('Los datos de acceso se modificaron correctamente', 'Modificando datos', { "closeButton": true });
                $("#refresh-dato-acceso").load(" #refresh-dato-acceso");
                $('#edit-dato-acceso').modal('hide');
                //$("#alerta-"+almId).fadeIn();
                $("#form-error").html('');
            }

            if (response.status == 'error') {
                $("#form-error").html(response.msj);
                $("#form-error").fadeIn();
            }
        }
    });
});

//Editamos los datos generales
$(".btnEditarDatosGeneral").click(function () {
    var path = $(this).data("url");

    var token = $("#token").val();
    var usrNombre = $('#usrNombre').val();
    var usrApellidoPaterno = $('#usrApellidoPaterno').val();
    var usrApellidoMaterno = $('#usrApellidoMaterno').val();
    var usrAlias = $('#usrAlias').val();
    var usrPuesto = $('#usrPuesto').val();

    $.ajax({
        url: path,
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        dataType: 'json',
        data: {
            usrNombre: usrNombre,
            usrApellidoPaterno: usrApellidoPaterno,
            usrApellidoMaterno: usrApellidoMaterno,
            usrAlias: usrAlias,
            usrPuesto: usrPuesto,
        },
        success: function (response) {
            //alert(response);
            console.log(response);
            //location.reload();
            //$("#error-email-tutor").fadeIn();

            if (response.status == 'success') {
                toastr.success('Los datos generales se modificaron correctamente', 'Modificando datos', { "closeButton": true });
                $("#refresh-dato-general").load(" #refresh-dato-general");
                $('#edit-dato-general').modal('hide');
                //$("#alerta-"+almId).fadeIn();
                $("#form-error-general").html('');
            }

            if (response.status == 'error') {
                $("#form-error-general").html(response.msj);
                $("#form-error-general").fadeIn();
            }
        }
    });

});

$(".isActiveUser").change(function () {
    var $data = $(this).prop("checked");
    var $data_url = $(this).data("url");

    if (typeof $data !== "undefined" && typeof $data_url !== "undefined") {
        $.get($data_url, { data: $data }, function (response) {
            $("#refresh-status").load(" #refresh-status");
            toastr.success('El estatus cambio correctamente', 'Modificando datos', { "closeButton": true });
        });
    }
});