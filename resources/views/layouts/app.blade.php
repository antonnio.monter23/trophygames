<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Sistema de Administración">
    <meta name="author" content="ANFAD">
    <title> TrophyGames- Dashboard </title>
    <link rel="apple-touch-icon" href="{{asset('assets/images-anfad/social-icon.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images-psn/platino.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/material-icons/material-icons.css')}}">

    <!-- META TAGS -->
    <meta property="og:site_name" content="ANFAD">
    <meta property="og:title" content="ANFAD" />
    <meta property="og:description" content="Sistema de Administración" />
    <meta property="og:image" itemprop="image" content="{{asset('assets/images-anfad/social-icon.png')}}">
    <meta property="og:type" content="website" />
    <meta property="og:updated_time" content="1440432930" />

    <meta name="csrf-token" content="{{ csrf_token() }}"> 

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/material-vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/weather-icons/climacons.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/meteocons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/charts/morris.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/charts/chartist.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/charts/chartist-plugin-tooltip.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/extensions/toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/forms/toggle/switchery.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/extensions/zoom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/ui/prism.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/file-uploaders/dropzone.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/js/gallery/photo-swipe/photoswipe.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/calendars/fullcalendar.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/calendars/daygrid.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/calendars/timegrid.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/material.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/material-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/material-colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/user-feed.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/menu/menu-types/material-vertical-content-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/simple-line-icons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/timeline.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/dashboard-ecommerce.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/extensions/toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/forms/switch.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/colors/palette-switch.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/file-uploaders/dropzone.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/dropzone.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/gallery.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/calendars/fullcalendar.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom.css')}}">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-content-menu material-vertical-layout material-layout 2-columns fixed-navbar color-body" data-open="click" data-menu="vertical-content-menu" data-col="2-columns">

    <style type="text/css">
        html body .pace .pace-progress {
            background: transparent; 
        }
        html body .content{
            min-height: calc(132% - 32px);
        }
        .color-body {
            background: linear-gradient(120deg, #4a4a4a 25%, #3e3e3e 25%, #3e3e3e 50%, #4a4a4a 50%, #4a4a4a 75%, #3e3e3e 75%, #3e3e3e) fixed;
        }
        .navbar-light {

            background: linear-gradient(120deg, #4a4a4a 25%, #4a4a4a 25%, #1162ac 50%, #4a4a4a 50%, #4a4a4a 75%, #1162ac 75%, #1162ac) fixed;
        }
        .header-navbar.navbar-light.navbar-border {
            border-bottom: 4px solid #000;
        }
        .header-navbar .navbar-header .navbar-brand .brand-logo {
            width: 80px;
            height: 80px;
            margin-top: -20px;
        }
        .material-vertical-layout .main-menu.menu-light {
            border: 2px solid #4a4a4a;
            box-shadow: 2px 2px 10px #000;
        }
        .material-vertical-layout .main-menu.menu-light .user-profile {
            background: linear-gradient(45deg, #4a4a4a 25%, #1162ac 25%, #4a4a4a 50%, #1162ac 50%, #4a4a4a 75%, #1162ac 75%, #4a4a4a);
        }
        .material-vertical-layout .main-menu .user-profile .user-info .user-img {
            border: 3px solid #fff6e8;
            width: 80px;
            height: 80px;
            margin-bottom: 0.5rem;
            box-shadow: 1px 1px 15px #000;
        }
        .user-name {
            font-size: 16px;
            font-weight: bold;
            color: #fff !important;
            text-shadow: 1px 1px 5px #000, 2px 2px 10px #000;
        }
        .user-rol {
            font-size: 16px;
            font-weight: bold;
            color: #fff6e8 !important;
            text-shadow: 1px 1px 5px #000;
        }
        .material-vertical-layout .main-menu .main-menu-content .navigation li.active > a {
            background: #ebd7bc !important;
            color: #a83419;
        }
        .main-menu.menu-light .navigation > li.active > a {
            margin: 0;
            border-radius: 0;
        }
        .main-menu.menu-light .navigation > li.open > a {
            color: #e94d2a;
            background: #fff6e8;
            border-right: 4px solid #c13b1c;
        }
        .main-menu.menu-light .navigation > li:hover > a{
            color: #e94d2a;
        }
        .nav-logout a:hover {
            color: #fff !important;
            background: #cc004e !important;
        }
        .card {
            border-radius: 0;
            border: 2px solid #fff6e8;
            box-shadow: 2px 2px 10px #000;
        }
        .card .card-header {
            background: #1162ac;
            color: #fff;
            font-weight: bold;
            padding: 10px;
        }
        .card-header:first-child {
            border-radius: 0;
        }
        .card .card-title{
            color: #fff;
            font-weight: bold;
        }
        footer.footer-light {
            background: #e6e7e9;
        }
        footer.navbar-border {
            border-top: 2px solid #e94d2a;
        }
    </style>

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-light navbar-hide-on-scroll navbar-border navbar-shadow navbar-brand-center">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item"><a class="navbar-brand" href=""><img class="brand-logo" alt="Games" src="{{asset('assets/images-psn/platino.png')}}">    
                        </a></li>
                    <li class="nav-item d-md-none d-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="material-icons mt-50">more_vert</i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
      
                    </ul>
                    <ul class="nav navbar-nav float-right">
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-header row">
        </div>
        <div class="content-overlay"></div>
        <div class="content-wrapper">

            <!-- BEGIN: Main Menu-->

            <div class="main-menu material-menu menu-static menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
                <div class="user-profile">
                    <div class="user-info text-center pt-1 pb-1">
                        <img class="user-img img-fluid rounded-circle" src="{{asset('assets/images-psn/platino.png')}}" />
                        <div class="name-wrapper d-block dropdown">
                            <span class="user-name">Administrador</span>
                            <div class="user-rol">Antonio Monter</div>
                        </div>
                    </div>
                </div>
                <div class="main-menu-content">
                    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                        <li class="nav-item {{ request()->is('home*') ? 'active' : ''}}">
                            <a href="">
                                <i class="material-icons">home</i>
                                <span class="menu-title">Inicio</span>
                            </a>
                        </li>

                        

                        

                        

                        

                        <li class="">
                            <a href="">
                                <i class="material-icons">supervisor_account</i>
                                <span class="menu-title">Usuarios</span>
                            </a>
                        </li>

                       

                        <!-- <li class="nav-logout nav-item">
                            <a href="">
                                <i class="material-icons">power_settings_new</i>
                                <span class="menu-title">Cerrar Sesión</span>
                            </a>
                        </li> -->
                    </ul>
                </div>
            </div>

            <!-- END: Main Menu-->
            <div class="content-body">
                @yield('contenido')  
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-border">
        <p class="clearfix lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">&copy;2021 TrophyGames</span><span class="float-md-right d-none d-lg-block">Platinos y logros de Plastation Network</span></p>
    </footer>
    <!-- END: Footer-->
    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('assets/vendors/js/material-vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/vendors/js/ui/headroom.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/charts/chartist.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/charts/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/charts/raphael-min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/charts/morris.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/timeline/horizontal-timeline.js')}}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/toggle/bootstrap-switch.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/toggle/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/transition.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/zoom.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/editors/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/dropzone.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/ui/prism.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/gallery/masonry/masonry.pkgd.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/gallery/photo-swipe/photoswipe.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/gallery/photo-swipe/photoswipe-ui-default.min.js')}}"></script>
    <script src="{{asset('assets/js/scripts/editors/editor-ckeditor.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/fullcalendar.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/interactions.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/daygrid.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/timegrid.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/gcal.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/locales-all.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/extensions/moment-timezone.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/js/scripts/pages/material-app.js')}}"></script>
    <script src="{{asset('assets/js/scripts/forms/switch.js')}}"></script>
    <script src="{{asset('assets/js/scripts/gallery/photo-swipe/photoswipe-script.js')}}"></script>
    <script src="{{asset('assets/js/evento-calendario.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script src="{{asset('assets/js/main-usuarios.js')}}"></script>
    <script src="{{asset('assets/js/main-directorios.js')}}"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>


    <script>
        var config = {
                apiKey: "{{ config('services.firebase.api_key') }}",
                authDomain: "{{ config('services.firebase.auth_domain') }}",
                databaseURL: "{{ config('services.firebase.database_url') }}",
                projectId: "{{ config('services.firebase.project_id') }}",
                storageBucket: "{{ config('services.firebase.storage_bucket') }}",
                messagingSenderId: "{{ config('services.firebase.messaging_sender_id') }}",
                appId: "{{ config('services.firebase.app_id') }}"
                };

            firebase.initializeApp(config);
            var database = firebase.database();
            var lastIndex = 0;
    </script>

<script src="./js/fb_crud.js"></script>

    <!-- END: Page JS-->

    @if (isset($impr) && $impr == '1')
        <script>
            $(document).ready(function () {
                toastr.{{$vary[0]}}(
                    '{{$vary[1]}}', 
                    '{{$vary[2]}}', 
                    {
                        "closeButton": true
                    }
                );
            });
        </script>
    @endif

</body>
<!-- END: Body-->

</html>