<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'firebase' => [
        'api_key' => 'AIzaSyBx9VHBEJTXo5xzwjGtBpdW9HhjcaFzzds',
        'auth_domain' => 'trophygames-e3b0f.firebaseapp.com',
        'database_url' => 'https://trophygames-e3b0f-default-rtdb.firebaseio.com',
        'project_id' => 'trophygames-e3b0f',
        'storage_bucket' => 'trophygames-e3b0f.appspot.com',
        'messaging_sender_id' => '460527905609',
        'app_id' => '1:460527905609:web:9a5a81851481f4afde8898',
    ],

];
