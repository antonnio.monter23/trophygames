$(document).ready((function() {
    var ruta_sitio = window.location.hostname;
    var path = '';
    if(ruta_sitio == '127.0.0.1'){
        path = 'http://127.0.0.1:8000/';
    } else {
        path = 'https://www.proyectosweb.mx/demos/anfad-admin/';
    }

    console.log(ruta_sitio);

    var d = new Date();
    var anio = d.getFullYear();
    var mes = (d.getMonth()+1);
    var dia = d.getDate();
    
    if(dia < 10) {
        dia = '0' + dia;
    }
    var strDate = anio + "-" + mes + "-" + dia;

    console.log(strDate);

    var calendarE8 = document.getElementById('calendario');
    var fcJson = new FullCalendar.Calendar(calendarE8, {
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
        defaultDate: strDate,
        locale: 'es',
        firstDay: 1,
		editable: true,
        eventLimit: true,
        plugins: ["dayGrid", "timeGrid", "interaction"],
		events: {
			url: path+'evento/all-calendar',
			error: function() {
				$('#script-warning').show();
			}
		},
		loading: function(bool) {
			$('#loading').toggle(bool);
        }
    });

    fcJson.render();

}));
    